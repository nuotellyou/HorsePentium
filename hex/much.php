<?php
/*这个脚本需要配合sendmail+crontab用
 *sendmail来处理发邮件
 *crontab定时计划来执行
 */
class much {
    public $isTradeTime,$min,$max,$now,$uprate,$open,$newPrice;
    public function __construct(){
        //获取行情内容
        $url = 'http://fa.163.com/interfaces/ngxcache/priceinfo/getRealTimePrice.do?systemVersion=5.1&jysId=pmec&systemName=m2+note&deviceType=android&uniqueId=869085029851749&goodsId=GDAG&productVersion=2.21.0&channel=netease&apiLevel=13&partnerId=pmec';
        $ret = json_decode(file_get_contents($url));
        //提取内容
        $this->isTradeTime = $ret->ret->isTradeTime;
        $this->min = $ret->ret->lowerPrice;
        $this->max = $ret->ret->highPrice;
        $this->now = $ret->ret->salePrice1;
        $this->uprate = $ret->ret->upRate;
        $this->open = $ret->ret->openPrice;
        $prevPrice = $_POST['prevPrice'];
        if ($ret->ret->newPrice != $prevPrice) {
            $this->newPrice = $ret->ret->newPrice;
            $this->setAvg();
        }
    }
    /**
     * @return [array] [获取并返回json数组]
     */
    public function getJson(){
        echo json_encode(array('now'=>$this->now,'min'=>$this->min,'max'=>$this->max,'uprate'=>$this->uprate,'open'=>$this->open));
    }

    /**
     * [setAvg 每次实例化对象就调用该方法写入当前价位]
     */
    public function setAvg(){
        setlocale(LC_ALL, 'en_US.UTF-8');
        $path = '/var/www/html/HorsePentium/money/moneyRecode/'.date('Y').'/'.date('m');
        if(!is_readable($path)){
            is_file($path) or mkdir($path,0777,true);
        }
        $file = fopen($path.'/'.date('d')."avgPrice.csv",'a+');
        $str = date('H:i:s').','.$this->newPrice;
        $str = mb_convert_encoding($str,'gb2312','utf-8');
        fwrite($file, $str."\r\n");
        fclose($file);
    }
}

$run = new much();
$run->getJson();
?>
